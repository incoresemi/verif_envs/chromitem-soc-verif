
**⚠ WARNING: This project has been moved to [gitlab.incoresemi.com](https://gitlab.incoresemi.com).
It will soon be archived and eventually deleted.**

# ChromiteM SoC Verification

This repo holds the test-cases to perform Soc-Level verification of the [Chromite-M SoC](https://chromitem-soc.readthedocs.io/)

## Adding tests

Follow the below steps to add a new test:
1. Create a directory under programs, say ``programs/pwm``.
2. Copy/create the test-case files under the above directory: ``programs/pwm/testcase1.c``
3. Create a makefile in the same directory: ``programs/pwm/Makefile``
4. To compile the test: ``make PROGRAM=testcase PROGRAMSDIR=programs/pwm``


