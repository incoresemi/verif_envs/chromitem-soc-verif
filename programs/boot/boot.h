char *bootlogo = "\n\
  _____        _____                \n\
 |_   _|      / ____|               \n\
   | |  _ __ | |     ___  _ __ ___  \n\
   | | | '_ \\| |    / _ \\| '__/ _ \\ \n\
  _| |_| | | | |___| (_) | | |  __/ \n\
 |_____|_| |_|\\_____\\___/|_|  \\___| \n\n";


char *soc_name ="\n\
            Chromite M SoC";

#define VERSIONPRE  "\n\
      Version: "
#define VERSIONVAL VERSIONPRE VERSION

#define BUILDPRE  "\n\
  Build Date: "
#define BUILDVAL BUILDPRE BUILDDATE

char *copyright="\n\
Copyright (c) 2021 InCore Semiconductors";
char *license = "\n\
  Available under Apache v2.0 License ";

#define Addr unsigned long
#define CHROMITEM_BOOT_CONFIG 0x200
#define CHROMITEM_GPIO_OUTPUT_EN 0x2000C
#define CHROMITEM_OCM_BASE 0x10000000
#define CHROMITEM_DDR_BASE 0x80000000

volatile Addr* ocm_base= (void*) CHROMITEM_OCM_BASE;
volatile Addr* ddr_base= (void*) CHROMITEM_DDR_BASE;

char boot_mode() {
  volatile char* boot_config_addr=(char*) (CHROMITEM_BOOT_CONFIG);
  return *boot_config_addr;
}


//char *bootlogo = "\n\
            CCCCCCCCCCCCCCCCCC\n\
         CCCCCCCCC      CCCCCCCC\n\
       CCCCCC  CCCCCCC       CCCCC\n\
     CCCCC  CCCCCCCCCC         CCCCC\n\
    CCCC  CCCCCC                 CCCC\n\
   CCCC CCCCC     CCC\n\
  CCCC CCCC   CCCCCCC\n\
 CCCC CCCC  CCCCCC\n\
 CCC CCCC CCCCC\n\
CCCC CCC  CCC\n\
CCC CCCC CCC\n\
CCC CCC  CCC                      CCC\n\
CCC CCC  CCC                      CCC\n\
CCC CCC  CCC                      CCC\n\
CCC CCCC CCCC                    CCCC\n\
CCCC CCC  CCCC                   CCC\n\
 CCC  CCC  CCCCC                CCCC\n\
  CCC CCCC   CCCCCCCC          CCCC\n\
  CCCC CCCCC   CCCCCC        CCCCC\n\
   CCCCC CCCCC             CCCCC  CCC\n\
     CCCC  CCCCCCCCCCCCCCCCCCCC CCCCC\n\
      CCCCC  CCCCCCCCCCCCCCC  CCCCC\n\
        CCCCCCC    CCC     CCCCCC\n\
          CCCCCCCCCCCCCCCCCCCCC\n\
              CCCCCCCCCCCCCC\n";
